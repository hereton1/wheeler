// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HorrorWheelGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HORRORWHEEL_API AHorrorWheelGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
